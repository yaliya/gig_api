<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gigs', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->dateTime('start')->default(now());
            $table->date('end');
            $table->unsignedInteger('positions');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('company_id');
            $table->double('hourly_rate')->default(0);
            $table->boolean('status')->default(\App\Domain\Gig\Models\Gig::STATUS_DRAFT);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gigs');
    }
}
