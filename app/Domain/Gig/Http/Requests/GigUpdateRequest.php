<?php

namespace App\Domain\Gig\Http\Requests;

use App\Domain\User\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class GigUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'sometimes|string',
            'status' => 'sometimes|in:0,1',
            'end' => 'sometimes|date|date_format:Y-m-d H:i',
            'start' => 'sometimes|date|date_format:Y-m-d H:i',
        ];
    }

    public function passedValidation()
    {
        $this->merge(['user_id' => Auth::id()]);
    }
}
