<?php

namespace App\Domain\Gig\Http\Requests;

use App\Domain\User\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class GigStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /**
         * @var User $user
         */
        $user = Auth::user();

        return (bool) $user->companies->where('id', '=', $this->company_id)->first();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'hourly_rate' => 'required',
            'status' => 'sometimes|in:0,1',
            'positions' => 'required|numeric',
            'company_id' => 'required|exists:companies,id',
            'end' => 'required|date|date_format:Y-m-d H:i',
            'start' => 'sometimes|date|date_format:Y-m-d H:i',
        ];
    }

    public function passedValidation()
    {
        $this->merge(['user_id' => Auth::id()]);
    }
}
