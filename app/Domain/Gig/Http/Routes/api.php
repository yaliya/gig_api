<?php

use Illuminate\Support\Facades\Route;
use App\Domain\Gig\Http\Controllers\GigController;

Route::apiResource('gig', GigController::class)->middleware('auth:api');
