<?php

namespace App\Domain\Gig\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class GigResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'end' => $this->end,
            'name' => $this->name,
            'start' => $this->start,
            'status' => $this->status,
            'positions' => $this->positions,
            'hourly_rate' => $this->hourly_rate,
            'company' => [
                'id' => $this->company->id,
                'name' => $this->company->name
            ]
        ];
    }
}
