<?php

namespace App\Domain\Gig\Http\Controllers;

use App\Domain\Gig\Models\Gig;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Domain\Gig\Http\Resources\GigResource;
use App\Domain\Gig\Repositories\GigRepository;
use App\Domain\Gig\Http\Requests\GigStoreRequest;
use App\Domain\Gig\Http\Requests\GigUpdateRequest;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * @property GigRepository $repository
 */
class GigController extends Controller
{
    public function __construct(GigRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(): AnonymousResourceCollection
    {
        return GigResource::collection($this->repository->index());
    }

    /**
     * @throws AuthorizationException
     */
    public function store(GigStoreRequest $request): GigResource
    {
        $this->authorize('store', Gig::class);

        $gig = $this->repository->create($request->all());

        return GigResource::make($gig);
    }

    /**
     * @throws AuthorizationException
     */
    public function update(GigUpdateRequest $request, Gig $gig): GigResource
    {
        $this->authorize('update', $gig);

        $this->repository->update($gig, $request->all());

        return GigResource::make($gig->refresh());
    }

    /**
     * @throws AuthorizationException
     */
    public function destroy(Gig $gig): JsonResponse
    {
        $this->authorize('delete', $gig);

        $this->repository->delete($gig);

        return response()->json('Deleted');
    }
}
