<?php

namespace App\Domain\Gig\Models;

use App\Domain\Company\Models\Company;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property mixed $user_id
 */
class Gig extends Model
{
    protected $fillable = [
        'end',
        'name',
        'start',
        'status',
        'user_id',
        'positions',
        'company_id',
        'hourly_rate',
    ];

    protected $with = [
        'company'
    ];

    protected $guarded = [
        'user_id',
        'company_id'
    ];

    const STATUS_DRAFT = 0;
    const STATUS_PUBLISHED = 1;

    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class);
    }
}
