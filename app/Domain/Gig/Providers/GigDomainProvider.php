<?php

namespace App\Domain\Gig\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class GigDomainProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mapApiRoutes();
        $this->mapWebRoutes();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    private function mapWebRoutes()
    {
        Route::prefix('gig')
            ->middleware('web')
            ->group(base_path('app/Domain/Gig/Http/Routes/web.php'));

    }

    private function mapApiRoutes()
    {
        Route::prefix('api')
            ->middleware('api')
            ->group(base_path('app/Domain/Gig/Http/Routes/api.php'));
    }
}
