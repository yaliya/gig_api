<?php

namespace App\Domain\Gig\Repositories;

use App\Domain\BaseRepository;
use App\Domain\Gig\Models\Gig;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Collection;

class GigRepository extends BaseRepository
{
    public function __construct(Gig $model)
    {
        parent::__construct($model);
    }

    public function index(): array|Collection
    {
        return $this->model->where('user_id', '=', Auth::id())->get();
    }
}
