<?php

namespace App\Domain\Gig\Policies;

use App\Domain\Company\Models\Company;
use App\Domain\Gig\Models\Gig;
use App\Domain\User\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class GigPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function store(User $user): bool
    {
        return true;
    }

    public function update(User $user, Gig $gig): bool
    {
        return $gig->user_id === $user->id;
    }

    public function delete(User $user, Gig $gig): bool
    {
        return $gig->user_id === $user->id;
    }
}
