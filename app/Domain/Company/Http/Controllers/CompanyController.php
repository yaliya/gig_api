<?php

namespace App\Domain\Company\Http\Controllers;

use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Domain\Company\Models\Company;
use Illuminate\Auth\Access\AuthorizationException;
use App\Domain\Company\Repositories\CompanyRepository;
use App\Domain\Company\Http\Resources\CompanyResource;
use App\Domain\Company\Http\Requests\CompanyStoreRequest;
use App\Domain\Company\Http\Requests\CompanyUpdateRequest;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * @property CompanyRepository $repository
 */
class CompanyController extends Controller
{
    public function __construct(CompanyRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(): AnonymousResourceCollection
    {
        return CompanyResource::collection($this->repository->index());
    }

    /**
     * @throws AuthorizationException
     */
    public function store(CompanyStoreRequest $request): CompanyResource
    {
        $this->authorize('store', Company::class);

        $company = $this->repository->create($request->all());

        return CompanyResource::make($company);
    }

    public function show(Company $company): CompanyResource
    {
        return CompanyResource::make($company);
    }

    /**
     * @throws AuthorizationException
     */
    public function update(CompanyUpdateRequest $request, Company $company): CompanyResource
    {
        $this->authorize('update', $company);

        $this->repository->update($company, $request->all());

        return CompanyResource::make($company->refresh());
    }

    /**
     * @throws AuthorizationException
     */
    public function destroy(Company $company): JsonResponse
    {
        $this->authorize('delete', $company);

        $this->repository->delete($company);

        return response()->json('Deleted');
    }
}
