<?php

use Illuminate\Support\Facades\Route;
use App\Domain\Company\Http\Controllers\CompanyController;

Route::apiResource('company', CompanyController::class)->middleware('auth:api');
