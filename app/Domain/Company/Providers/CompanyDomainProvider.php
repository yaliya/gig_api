<?php

namespace App\Domain\Company\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class CompanyDomainProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mapApiRoutes();
        $this->mapWebRoutes();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    private function mapWebRoutes()
    {
        Route::prefix('gig')
            ->middleware('web')
            ->group(base_path('app/Domain/Company/Http/Routes/web.php'));

    }

    private function mapApiRoutes()
    {
        Route::prefix('api')
            ->middleware('api')
            ->group(base_path('app/Domain/Company/Http/Routes/api.php'));
    }
}
