<?php

namespace App\Domain\Company\Repositories;

use App\Domain\BaseRepository;
use App\Domain\Company\Models\Company;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;

class CompanyRepository extends BaseRepository
{
    public function __construct(Company $model)
    {
        parent::__construct($model);
    }

    public function index(): array|Collection
    {
        return $this->model->where('user_id', '=', Auth::id())->get();
    }
}
