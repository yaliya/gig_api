<?php

namespace App\Domain\Company\Models;

use App\Domain\Gig\Models\Gig;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed $user_id
 */
class Company extends Model
{
    protected $fillable = [
        'name',
        'user_id'
    ];

    protected $guarded = [
        'user_id'
    ];

    protected $withCount = [
        'gigs'
    ];

    protected $appends = [
        'started_gigs'
    ];

    public function gigs(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Gig::class);
    }

    public function getStartedGigsAttribute()
    {
        return $this->gigs->where('start', '>', now())->count();
    }
}
