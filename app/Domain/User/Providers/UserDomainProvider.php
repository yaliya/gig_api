<?php

namespace App\Domain\User\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class UserDomainProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mapApiRoutes();
        $this->mapWebRoutes();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    private function mapWebRoutes()
    {
        Route::prefix('user')
            ->middleware('web')
            ->group(base_path('app/Domain/User/Http/Routes/web.php'));

    }

    private function mapApiRoutes()
    {
        Route::prefix('api')
            ->middleware('api')
            ->group(base_path('app/Domain/User/Http/Routes/api.php'));
    }
}
