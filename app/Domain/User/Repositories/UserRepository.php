<?php

namespace App\Domain\User\Repositories;

use App\Domain\BaseRepository;
use App\Domain\User\Models\User;

class UserRepository extends BaseRepository
{
    public function __construct(User $model)
    {
        parent::__construct($model);
    }
}
