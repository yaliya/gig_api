<?php

use Illuminate\Support\Facades\Route;
use App\Domain\User\Http\Controllers\UserController;

Route::apiResource('user', UserController::class)->except(['index', 'delete']);
