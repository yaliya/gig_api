<?php

namespace App\Domain\User\Http\Controllers;

use App\Domain\User\Http\Resources\UserResource;
use App\Domain\User\Repositories\UserRepository;
use App\Http\Controllers\Controller;
use App\Domain\User\Http\Requests\UserStoreRequest;

/**
 * @property UserRepository $repository
 */
class UserController extends Controller
{
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function store(UserStoreRequest $request): UserResource
    {
        $user = $this->repository->create($request->all());

        return UserResource::make($user);
    }
}
