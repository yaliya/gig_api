<?php

namespace App\Domain;

use Illuminate\Database\Eloquent\Model;

interface RepositoryInterface
{
    public function create(array $data);

    public function update(array $data, Model $model);

    public function delete(Model $model);
}
