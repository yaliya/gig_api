<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class PostRate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'post:rate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Post Rate calculator';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        DB::statement('UPDATE users set post_rate = IFNULL(NULLIF((SELECT COUNT(id) from gigs WHERE user_id=users.id AND status=1), 0) / (SELECT COUNT(id) from gigs WHERE user_id=users.id), 0) * 100');

        return true;
    }
}
